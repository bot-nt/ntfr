# Projet de traduction d'Index : Nouveau Testament

## Balisage

* `TOME:<no>` indique le numéro du tome
* `CHAPITRE:<no>`, `PROLOGUE!` ou `EPILOGUE!` indique le numéro du chapitre
* `TITRE:<titre>` indique le titre du chapitre
* `SOUS-TITRE:<titre>` indique le titre alternatif du chapitre
* `PARTIE:<no>` indique un changement de partie
* `/  /` texte en italique
* `{  }` commentaire
* `[...]` à compléter
* `[!  !]` à corriger
* `_` espace insécable
* `***` séparation de sous-partie
* `---` saut de ligne
* `^` exposant
