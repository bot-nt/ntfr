var additionalInfoElelement, rootElement, contentsButtonElement;

function onLoad() {
	rootElement = document.documentElement;
	additionalInfoElelement = document.getElementById('additional-info');
	contentsButtonElement = document.getElementById('contents-button');

	document.addEventListener(
		'scroll',
		(ev) => {
			if (window.scrollY > 130) {
				//rootElement.style.setProperty('--accent-color', '#6e6e6e');
				additionalInfoElelement.innerText = '> ' + CHAPTERNAME + ' > Partie ?';
			} else {
				//rootElement.style.setProperty('--accent-color', '#1080c9');
				additionalInfoElelement.innerText = '';
			}
			contentsButtonElement.style.top = Math.max(190, (window.scrollY + 60)) + 'px'
		}, 
		{ passive: true }
	);
}