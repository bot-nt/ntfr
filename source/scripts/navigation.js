var additionalInfoElelement;

function onLoad() {
	additionalInfoElelement = document.getElementById('additional-info');

	document.addEventListener(
		'scroll',
		(ev) => {
			if (window.scrollY > 50) {
				additionalInfoElelement.innerText = 'Nouveau Testament ' + VOLUMENAME;
			} else {
				additionalInfoElelement.innerText = '';
			}
		}, 
		{ passive: true }
	);
}