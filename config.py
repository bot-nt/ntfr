# build output directory
BUILD_DIR = "site/"

# templates source directory
TEMPLATES_DIR = "source/templates/"

# chapters source directory
CHAPTERS_DIR = "source/chapters/"

# css source directory
STYLESHEETS_DIR = "source/style/"

# js source directory
SCRIPTS_DIR = "source/scripts/"

# images directory
RESOURCES_DIR = "source/resources/"

REPOSITORY_URL = "https://gitlab.com/raildexfr/ntfr/-/blob/main/"
