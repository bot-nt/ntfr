import os, shutil, re, sys
import yaml

# prevent the old site to deploy
sys.argv[1]

from config import (
    BUILD_DIR,
    TEMPLATES_DIR,
    CHAPTERS_DIR,
    STYLESHEETS_DIR,
    SCRIPTS_DIR,
    REPOSITORY_URL,
    RESOURCES_DIR,
)

STATIC_DIR = os.path.join(BUILD_DIR, "static/")

VOLUME_TAG = "TOME:"
CHAPTER_TAG = "CHAPITRE:"
TITLE_TAG = "TITRE:"
SUBTITLE_TAG = "SOUS-TITRE:"
SECTION_TAG = "PARTIE:"
PROLOGUE_TAG = "PROLOGUE!"

MARKUP = (
    (r"_", "&nbsp;"),
    (r"\B/\b", "<em>"),
    (r"\b/\B", "</em>"),
    (r"\B\[!\s+", '<span class="wrong">'),
    (r"\s+!\]\B", "</span>"),
    (r"\B\[\.\.\.\]\B", '<span class="missing">[…]</span>'),
    (r"\B{\B", '<span class="comment">'),
    (r"\B}\B", "</span>"),
    (r"(\^)(\w+?\b)", r"<sup>\2</sup>"),
)


def markup(text):
    for regex, html in MARKUP:
        text = re.sub(regex, html, text)
    return text


class Templates:
    def __init__(self):
        self._dict = dict()

    def add(self, name, html):
        self._dict[name] = html

    def __getattr__(self, attr):
        return self._dict[attr]


def load_templates_into(tobj):
    for file in os.listdir(TEMPLATES_DIR):
        path = os.path.join(TEMPLATES_DIR, file)
        name = os.path.splitext(file)[0].upper()
        with open(path, "rt", encoding="utf-8") as fd:
            tobj.add(name, fd.read())


def clear_previous_build():
    if os.path.isdir(BUILD_DIR):
        shutil.rmtree(BUILD_DIR)


def create_tree():
    os.mkdir(BUILD_DIR)
    os.mkdir(os.path.join(BUILD_DIR, "static/"))


def copy_stylesheets():
    for file in os.listdir(STYLESHEETS_DIR):
        with open(os.path.join(STYLESHEETS_DIR, file), "rt", encoding="utf-8") as fd:
            original = fd.read()

        minified = original

        output_path = os.path.join(STATIC_DIR, os.path.splitext(file)[0] + ".min.css")
        with open(output_path, "wt+", encoding="utf-8") as fd:
            fd.write(minified)


def copy_scripts():
    for file in os.listdir(SCRIPTS_DIR):
        with open(os.path.join(SCRIPTS_DIR, file), "rt", encoding="utf-8") as fd:
            original = fd.read()

        minified = original

        output_path = os.path.join(STATIC_DIR, os.path.splitext(file)[0] + ".min.js")
        with open(output_path, "wt+", encoding="utf-8") as fd:
            fd.write(minified)


def copy_resources():
    for file in os.listdir(RESOURCES_DIR):
        source_path = os.path.join(RESOURCES_DIR, file)
        destination_path = os.path.join(STATIC_DIR, file)
        shutil.copyfile(source_path, destination_path)


def generate_chapters():
    volumes = dict()

    for file in os.listdir(CHAPTERS_DIR):
        path = os.path.join(CHAPTERS_DIR, file)

        with open(path, "rt", encoding="utf-8") as fd:
            source = fd.readlines()

        volume = None
        chapter = None
        chaptername = None
        title = None
        subtitle = None

        first_section = True

        text = str()

        for line in source:
            if line.isspace():
                continue
            elif line.startswith(VOLUME_TAG):
                volume = line.split(":", 1)[1].strip()
            elif line.startswith(CHAPTER_TAG):
                chapter = line.split(":", 1)[1].strip()
                chaptername = f"Chapitre {chapter}"
            elif line.startswith(PROLOGUE_TAG):
                chapter = "P"
                chaptername = f"Prologue"
            elif line.startswith(TITLE_TAG):
                title = line.split(":", 1)[1].strip()
            elif line.startswith(SUBTITLE_TAG):
                subtitle = line.split(":", 1)[1].strip()
            elif line.startswith(SECTION_TAG):
                if not first_section:
                    text += "</section><section>"
                else:
                    first_section = False
                text += f'<h2>—&nbsp;{line.split(":", 1)[1].strip()}&nbsp;—</h2>'
            else:
                text += f"<p>{markup(line)}</p>"

        assert volume is not None
        assert chapter is not None
        assert chaptername is not None
        assert title is not None

        if volume in volumes:
            volumes[volume].append(chapter)
        else:
            volumes[volume] = [chapter]

        html = (
            TEMPLATE.DOCUMENT.replace("$PAGENAME", f"Tome {volume}, {chaptername}")
            .replace("$SOURCE", REPOSITORY_URL + "source/chapters/" + file)
            .replace("$STYLE", "../static/content.min.css")
            .replace("$SCRIPT", "../static/content.min.js")
            .replace("$BODY", TEMPLATE.CHAPTER)
            .replace("$CHAPTER", chaptername)
            .replace("$VOLUME", volume)
            .replace("$PARENT", f"../tome-{volume}")
            .replace("$TITLE", title)
            .replace("$SUBTITLE", subtitle if subtitle is not None else "")
            .replace("$TEXT", text)
        )

        voldir = os.path.join(BUILD_DIR, f"tome-{volume}")
        if not os.path.isdir(voldir):
            os.mkdir(voldir)

        with open(
            os.path.join(
                voldir,
                "prologue.html" if chapter == "P" else f"chapitre-{chapter}.html",
            ),
            "wt+",
            encoding="utf-8",
        ) as fd:
            fd.write(html)

    return volumes


def generate_volumes(volumes):
    for vol in volumes:
        chapters_html = "".join(
            [
                f'<li><a href="tome-{vol}/chapitre-{n}">Chapitre {n}</a></li>'
                for n in volumes[vol]
            ]
        )

        volume = (
            TEMPLATE.DOCUMENT.replace("$PAGENAME", f"Tome {vol}")
            .replace("$SOURCE", REPOSITORY_URL + "source/templates/volume.html")
            .replace("$STYLE", "./static/navigation.min.css")
            .replace("$SCRIPT", "./static/navigation.min.js")
            .replace("$BODY", TEMPLATE.VOLUME)
            .replace("$VOLUME", vol)
            .replace("$CHAPTERS", chapters_html)
            .replace("$PARENT", ".")
        )

        with open(
            os.path.join(BUILD_DIR, f"tome-{vol}.html"), "wt+", encoding="utf-8"
        ) as fd:
            fd.write(volume)


def generate_index(volumes):

    volumes_html = "".join(
        [
            f'<a href="tome-{n}" class="book" aria-label="Tome {n}" {book_gradient(n)}>'
            '<img src="static/title-sideways.svg" class="title"></img></a>'
            for n in sorted(volumes.keys(), key=lambda v: v.zfill(2))
        ]
    )

    index = (
        TEMPLATE.DOCUMENT.replace("$PAGENAME", "Accueil")
        .replace("$SOURCE", REPOSITORY_URL + "source/templates/homepage.html")
        .replace("$STYLE", "./static/homepage.min.css")
        .replace("$SCRIPT", "homepage.min.js")
        .replace("$BODY", TEMPLATE.HOMEPAGE)
        .replace("$VOLUMES", volumes_html)
    )

    with open(os.path.join(BUILD_DIR, "index.html"), "wt+", encoding="utf-8") as fd:
        fd.write(index)


def book_gradient(book_id):
    if book_id in VOLUME_INFO:
        return (
            'style="background: linear-gradient(0deg, white 0%,'
            f'#{VOLUME_INFO[book_id]["color"]} 100%)"'
        )
    else:
        return ""


if __name__ == "__main__":
    print("[BUILDSITE]")

    print("loading templates ... ", end="")
    TEMPLATE = Templates()
    load_templates_into(TEMPLATE)
    print("done")

    print("loading additional data ... ", end="")
    with open(os.path.join(RESOURCES_DIR, "volumeinfo.yaml"), "rt") as fd:
        VOLUME_INFO = yaml.load(fd.read(), Loader=yaml.FullLoader)
    print("done")

    print("removing any previous builds ... ", end="")
    clear_previous_build()
    print("done")
    print("generating base tree ... ", end="")
    create_tree()
    print("done")

    print("copying style sheets ... ", end="")
    copy_stylesheets()
    print("done")

    print("copying scripts ... ", end="")
    copy_scripts()
    print("done")

    print("copying resources ... ", end="")
    copy_resources()
    print("done")

    print("generating chapter pages ... ", end="")
    vols = generate_chapters()
    print("done")
    print("generating volume pages ... ", end="")
    generate_volumes(vols)
    print("done")
    print("generating homepage ... ", end="")
    generate_index(vols)
    print("done")
